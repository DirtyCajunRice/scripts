# scripts

All my customized scripts for my Ubuntu Server.
This repository is a collection of the scripts 
that need to be customized to have a well rounded 
fully functioning ubuntu server from home. I have
included a changelog that documents the entire 
process from beginning to end. if you have any
questions feel free to open an issue ill get back
to you as quickly as i can. This is also my attempt
to completely rely on systemd and have no upstart 
scripts on my server whatsoever. if you follow my 
how-to.info you should be able to go from start 
to finish in less than 3 hours. 

I hold no licence on any of these products they are
all all freely distributable.

